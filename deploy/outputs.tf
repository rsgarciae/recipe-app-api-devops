output "db_host" {
  value = aws_db_instance.main.address
}

output "bastion_host" {
  value = aws_instance.bastion.public_dns
}

output "api_endpoint" {
  # on default
  # value = aws_lb.api.dns_name
  #On custom DNS
  value = aws_route53_record.app.fqdn
}
